package ru.t1.amsmirnov.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.command.project.AbstractProjectCommand;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectClearRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerDropSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectClearResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerDropSchemeResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class ServerDropSchemeCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "drop-scheme";

    @NotNull
    public static final String DESCRIPTION = "Drop server database.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DROP SERVER DATABASE]");
        @NotNull final ServerDropSchemeRequest request = new ServerDropSchemeRequest(getToken());
        @NotNull final ServerDropSchemeResponse response = getServiceLocator().getSystemEndpoint().dropScheme(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
