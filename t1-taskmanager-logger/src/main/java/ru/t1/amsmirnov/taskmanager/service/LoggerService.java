package ru.t1.amsmirnov.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.ILoggerService;
import ru.t1.amsmirnov.taskmanager.dto.log.OperationEvent;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public final class LoggerService implements ILoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @Override
    public void log(@NotNull final String message) throws IOException {
        @NotNull final OperationEvent event = objectMapper.readValue(message, OperationEvent.class);
        @NotNull final String table = "log" + File.separator + event.getTableName() + ".log";
        @NotNull final byte[] bytes = message.getBytes();
        @NotNull final File file = new File(table);
        file.getParentFile().mkdirs();
        file.createNewFile();
        Files.write(Paths.get(table), bytes, StandardOpenOption.APPEND);
    }

}
