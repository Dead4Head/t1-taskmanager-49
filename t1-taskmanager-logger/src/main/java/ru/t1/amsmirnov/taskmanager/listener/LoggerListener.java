package ru.t1.amsmirnov.taskmanager.listener;

import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.ILoggerService;
import ru.t1.amsmirnov.taskmanager.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class LoggerListener implements MessageListener {

    @NotNull
    private final static Logger logger = Logger.getLogger(LoggerListener.class);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Override
    public void onMessage(final Message message) {
        try {
            if (!(message instanceof TextMessage)) return;
            @NotNull final TextMessage textMessage = (TextMessage) message;
            @NotNull final String yaml = textMessage.getText();
            loggerService.log(yaml);
        } catch (final Exception exception) {
            logger.error(exception);
        }
    }

}
