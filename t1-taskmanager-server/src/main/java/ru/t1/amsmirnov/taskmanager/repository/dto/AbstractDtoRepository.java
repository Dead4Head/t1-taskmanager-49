package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IAbstractDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IAbstractDtoRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDtoRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final AbstractModelDTO model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final AbstractModelDTO model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final AbstractModelDTO model) {
        entityManager.remove(model);
    }

}
